"""Analiza_piosenek URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

from organizer import views as organizer_views
from users import views as users_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', users_views.register, name='user-register'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='user-login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='user-logout'),
    path('profile/', users_views.profile, name='user-profile'),
    path('upload/', users_views.profile, name='upload'),
    path('', organizer_views.home, name='mjuzik-home'),
    path('about/', organizer_views.about, name='mjuzik-about'),
    path('authors/', organizer_views.authors, name='mjuzik-authors'),
    path('songs/', organizer_views.songs, name='mjuzik-songs'),
    path('lajk_author/', organizer_views.lajk_author, name='lajk_author'),
    path('lajk_song/', organizer_views.lajk_song, name='lajk_song')

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
