#!/usr/bin/env python
import psycopg2

from Analiza_piosenek.settings import DATABASES


_QUERY_TRIGGER_CREATION = """
BEGIN;
CREATE OR REPLACE FUNCTION function_author_counter() RETURNS TRIGGER AS $$
   BEGIN
      DROP SEQUENCE IF EXISTS author_counter;
      RETURN NULL;
   END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS sequence_author_counter ON organizer_author;

CREATE TRIGGER sequence_author_counter
BEFORE UPDATE ON organizer_author
EXECUTE PROCEDURE function_author_counter();

CREATE OR REPLACE FUNCTION function_songs_counter() RETURNS TRIGGER AS $$
   BEGIN
      DROP SEQUENCE IF EXISTS songs_counter;
      RETURN NULL;
   END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS sequence_songs_counter ON organizer_songs;

CREATE TRIGGER sequence_songs_counter
BEFORE UPDATE ON organizer_songs
EXECUTE PROCEDURE function_songs_counter();
END;
"""

db_creds = DATABASES['default']

conn = psycopg2.connect(dbname=db_creds['NAME'], user=db_creds['USER'], password=db_creds['PASSWORD'],
                        host=db_creds['HOST'], port=db_creds['PORT'])
with conn.cursor() as cursor:
    if cursor.execute(_QUERY_TRIGGER_CREATION) is None:
        print('SUCCESS')
    else:
        print("FAIL")
