from django.shortcuts import render, redirect
from .filters import Searcher
from .forms import SearchFrom
from .models import Author, Songs
from users.models import Profile
from organizer.analyzer import lyrics_analyzer, lyric_downloader
from database_resources.queries import QUERY_TOP_AUTHORS, QUERY_TOP_SONGS, QUERY_TOP_AUTHORS_USER, QUERY_TOP_SONGS_USER
from django_postgres_extensions.models.functions import ArrayRemove, ArrayAppend


def home(request):
    if request.method == 'POST':
        return _handle_search_request(request)
    elif request.method == 'GET':
        picked_author = _handle_author_request(request)
        if picked_author:
            return handle_base_cards(request, 'authors/authors_joiner.html', {'picked_author': picked_author})
        picked_song = _handle_song_request(request)
        if picked_song:
            return handle_base_cards(request, 'songs/songs_joiner.html', {'picked_song': picked_song})
        form = SearchFrom()
    else:
        form = SearchFrom()
    return handle_base_cards(request, 'home.html', {'search_from': form})


def about(request):
    if request.method == 'POST':
        return _handle_search_request(request)
    elif request.method == 'GET':
        song_or_author_request = go_to_song_or_author(request)
        if song_or_author_request:
            return handle_base_cards(request, song_or_author_request[0], song_or_author_request[1])
    return handle_base_cards(request, 'about.html', {'title': 'About'})


def authors(request):
    if request.method == 'POST':
        return _handle_search_request(request)
    elif request.method == 'GET':
        song_or_author_request = go_to_song_or_author(request)
        if song_or_author_request:
            return handle_base_cards(request, song_or_author_request[0], song_or_author_request[1])
    top_authors = Songs.objects.raw(QUERY_TOP_AUTHORS.format(limit=10))
    return handle_base_cards(request, 'authors/authors_joiner.html', {'title': 'Authors',
                                                                      'top_authors': top_authors})


def songs(request):
    if request.method == 'POST':
        return _handle_search_request(request)
    elif request.method == 'GET':
        song_or_author_request = go_to_song_or_author(request)
        if song_or_author_request:
            return handle_base_cards(request, song_or_author_request[0], song_or_author_request[1])
    top_songs = Songs.objects.raw(QUERY_TOP_SONGS.format(limit=10))
    return handle_base_cards(request, 'songs/songs_joiner.html', {'title': 'Songs',
                                                                  'top_songs': top_songs})


def _handle_search_request(request):
    form = SearchFrom(request.POST)
    if form.is_valid():
        searched_phrase = form.cleaned_data.get('searcher', None)
        download_searcher = request.POST.get('download_search', 0)
        if searched_phrase is not None and len(searched_phrase) > 1:
            author_results, songs_results = Searcher().search_phrase(searched_phrase, download_searcher, limit=25)
            return handle_base_cards(request, 'home.html', {'search_from': form,
                                                            'author_results': author_results,
                                                            'songs_results': songs_results,
                                                            'searched': True})
    return handle_base_cards(request, 'home.html', {'search_from': form, 'searched': True})


def _handle_author_request(request):
    if request.GET.get("author"):
        picked_author = request.GET.get("author")
        try:
            author_row = Author.objects.get(genius_id=picked_author)
        except Author.DoesNotExist:
            return None
        author_row.views += 1
        author_row.save()
        if request.user.is_authenticated:
            profile = Profile.objects.get(user_id=request.user)
            if picked_author in profile.authors_views:
                profile.authors_views[picked_author] = 1 + int(profile.authors_views[picked_author])
            else:
                profile.authors_views[picked_author] = 1
            profile.save()
        return author_row


def _handle_song_request(request):
    if request.GET.get("song"):
        picked_song = request.GET.get("song")
        try:
            song_row = Songs.objects.get(genius_id=picked_song)
        except Songs.DoesNotExist:
            return None
        song_row.views += 1
        song_row.save()
        if request.user.is_authenticated:
            profile = Profile.objects.get(user_id=request.user)
            if picked_song in profile.songs_views:
                profile.songs_views[picked_song] = 1 + int(profile.songs_views[picked_song])
            else:
                profile.songs_views[picked_song] = 1
            profile.save()
        return song_row


def handle_base_cards(*args):
    the_best_authors = Author.objects.raw(QUERY_TOP_AUTHORS.format(limit=3))
    the_best_songs = Songs.objects.raw(QUERY_TOP_SONGS.format(limit=3))
    check = 'no'
    args[2]['the_best_authors'] = the_best_authors
    args[2]['the_best_songs'] = the_best_songs
    args[2]['check'] = check
    if args[0].user.is_authenticated:
        _handle_user_base_cards(args)
    return render(args[0], args[1], args[2])


def _handle_user_base_cards(args):
    user_id = args[0].user.id
    args[2]['the_best_user_authors'] = Author.objects.raw(QUERY_TOP_AUTHORS_USER.format(limit=3, user_id=user_id))
    args[2]['the_best_user_songs'] = Songs.objects.raw(QUERY_TOP_SONGS_USER.format(limit=3, user_id=user_id))


def go_to_song_or_author(request):
    picked_author = _handle_author_request(request)
    picked_song = _handle_song_request(request)
    if picked_author:
        authors_likes_number = Profile.objects.filter(liked_authors__contains=[picked_author.pk]).count()
        if request.user.is_authenticated:
            checkbox_author = _check_if_liked_author(request, picked_author.genius_id)
        else:
            checkbox_author = None
        picked_author_song = Songs.objects.filter(main_author=picked_author).order_by('-published_date')[0:9]
        return ['authors/authors_joiner.html', {'picked_author': picked_author,
                                                'picked_author_song': picked_author_song,
                                                'checkbox_author': checkbox_author,
                                                'authors_likes_number': authors_likes_number}]
    if picked_song:
        songs_likes_number = Profile.objects.filter(liked_songs__contains=[picked_song.pk]).count()
        if request.user.is_authenticated:
            checkbox_song = _check_if_liked_song(request, picked_song.genius_id)
        else:
            checkbox_song = None
        author = Author.objects.filter(genius_id=picked_song.main_author_id)[0]
        featured_authors = Author.objects.filter(nickname__in=picked_song.featured_authors_nicknames)
        similarity_pic = lyrics_analyzer.create_similarity_image(
            lyric_downloader.GeniusAPI.parse_lyrics(picked_song.lyrics), picked_song.title, picked_song.lyrics)

        return ['songs/songs_joiner.html', {'picked_song': picked_song, 'author': author,
                                            'featured_authors': featured_authors,
                                            'checkbox_song': checkbox_song,
                                            'songs_likes_number': songs_likes_number,
                                            'similarity_pic': similarity_pic}]


def _check_if_liked_author(request, picked_author):
    liked_author = Profile.objects.filter(liked_authors__contains=[picked_author], user__exact=request.user).first()
    if liked_author:
        checkbox_author = True
    else:
        checkbox_author = False
    return checkbox_author


def _check_if_liked_song(request, picked_song):
    liked_song = Profile.objects.filter(liked_songs__contains=[picked_song], user__exact=request.user).first()
    if liked_song:
        checkbox_song = True
    else:
        checkbox_song = False
    return checkbox_song


def lajk_author(request):
    author_id = request.POST.get('like_author_id')
    liked_author = _check_if_liked_author(request, author_id)
    if liked_author:
        Profile.objects.filter(user__exact=request.user).update(liked_authors=ArrayRemove('liked_authors', author_id))
    else:
        Profile.objects.filter(user__exact=request.user).update(liked_authors=ArrayAppend('liked_authors', author_id))
    return redirect(request.META['HTTP_REFERER'])


def lajk_song(request):
    song_id = request.POST.get('like_song_id')
    liked_song = _check_if_liked_song(request, song_id)
    if liked_song:
        Profile.objects.filter(user__exact=request.user).update(liked_songs=ArrayRemove('liked_songs', song_id))
    else:
        Profile.objects.filter(user__exact=request.user).update(liked_songs=ArrayAppend('liked_songs', song_id))
    return redirect(request.META['HTTP_REFERER'])
