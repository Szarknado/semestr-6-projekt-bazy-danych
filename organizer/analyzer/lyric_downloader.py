import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import gevent
import json
from bs4 import BeautifulSoup
from functools import reduce
from itertools import product
from more_itertools import unique_everseen
import logging
from ..models import OmittedData, Author, Songs

from database_resources.database_inserter import insert_data_to_database

logger = logging.getLogger(__name__)


def handle_response(load_json):
    def handle_response_dec(f):
        def func_wrapper(*args, **kwargs):
            response = f(*args, **kwargs)
            if response.status_code == 200:
                return json.loads(response.content)['response'] if load_json else response.content
            else:
                logger.warn(f"Download for url '{response.url}' was unsuccessful. "
                            f"Ended with code: {response.status_code}. "
                            f"Message: {response.reason}")
                return None

        return func_wrapper

    return handle_response_dec


class GetMetadata:
    URL = "https://genius.com"
    API_URL = "https://api.genius.com"
    TOKEN = "Z2ITyikNSObTEHWAnUNuvLRU-0KwLm11EqrWfJR-JnLiy0aDN7QCsct4LNfovDJk"

    @staticmethod
    def requests_retry_session(retries=3, backoff_factor=0.3):
        session = requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    @staticmethod
    @handle_response(True)
    def make_request_json(url):
        return GetMetadata.requests_retry_session().get(url, headers={'Authorization': f"Bearer {GetMetadata.TOKEN}"})

    @staticmethod
    @handle_response(False)
    def make_request_html(url):
        return GetMetadata.requests_retry_session().get(url, headers={'Authorization': f"Bearer {GetMetadata.TOKEN}"})

    @staticmethod
    def async_requests(urls, join_all):
        jobs = [gevent.spawn(GetMetadata.make_request_json, url) for url in urls]
        if join_all:
            return GetMetadata.join_jobs(jobs)
        else:
            return jobs

    @staticmethod
    def join_jobs(jobs):
        joined_response = gevent.joinall(jobs)
        return [i.value for i in joined_response if i.value]

    @staticmethod
    def search(search_names, join_all):
        if search_names:
            return GetMetadata.async_requests([f"{GetMetadata.API_URL}/search?q={search_name}&per_page=50&page={page}"
                                               for search_name, page in product(search_names, range(200))], join_all)
        return []

    @staticmethod
    def author(author_ids, join_all):
        if author_ids:
            return GetMetadata.async_requests([f"{GetMetadata.API_URL}/artists/{author_id}"
                                               for author_id in author_ids], join_all)
        return []

    @staticmethod
    def song(song_ids, join_all):
        if song_ids:
            return GetMetadata.async_requests([f"{GetMetadata.API_URL}/songs/{song_id}"
                                               for song_id in song_ids], join_all)
        return []


class GeniusAPI:
    @staticmethod
    def download_lyrics(url):
        return GetMetadata.make_request_html(url)

    @staticmethod
    def get_lyrics_list(url):
        resp = GeniusAPI.download_lyrics(url)
        if resp is not None:
            lyrics = BeautifulSoup(resp, features="html.parser").body.find('div', attrs={'class': 'lyrics'})
            if hasattr(lyrics, 'text'):
                return lyrics.text
            else:
                logger.warn(f"No lyrics found for url: '{url}'")
                return ''
        return None, None

    @staticmethod
    def parse_lyrics(lyrics):
        return ' '.join(reduce(lambda x, y: x + y, map(str.split, lyrics.split('\n'))))

    @staticmethod
    def search_songs(search_phrase, new_entries_limit=50):
        genius = GetMetadata()
        search_results = genius.search([search_phrase.capitalize()], True)
        if search_results:
            search_results = [i.get('result') for search_result in search_results
                              for i in search_result.get('hits') if i]
            unknown_authors = genius.author(list(unique_everseen(
                result.get('primary_artist').get('id') for result in search_results
                if result.get('primary_artist')
                and not Author.objects.filter(genius_id__exact=result.get('primary_artist', {}).get('id'))
            ))[:new_entries_limit], True)
            unknown_songs_jobs = genius.song(list(unique_everseen(
                result.get('id') for result in search_results
                if not OmittedData.objects.filter(data_type='song', genius_id=result.get('id'))
                and not Songs.objects.filter(genius_id__exact=result.get('id'))
                and result.get('primary_artist')
                and Author.objects.filter(genius_id=result.get('primary_artist').get('id'))
            ))[:new_entries_limit], False)
            if unknown_authors:
                parsed_unknown_authors = GeniusAPI._parse_authors(unknown_authors)
                insert_data_to_database(Author, parsed_unknown_authors)
            else:
                parsed_unknown_authors = []
            unknown_songs = GetMetadata.join_jobs(unknown_songs_jobs)
            if unknown_songs:
                parsed_unknown_songs = GeniusAPI._parse_songs(unknown_songs)
                parsed_unknown_songs = GeniusAPI._handle_ommited_values('song', parsed_unknown_songs)
                insert_data_to_database(Songs, parsed_unknown_songs)
            else:
                parsed_unknown_songs = []
            return parsed_unknown_authors, parsed_unknown_songs
        return [], []

    @staticmethod
    def _parse_songs(songs):
        threads = [gevent.spawn(GeniusAPI._parse_one_song, song.get('song'))
                   for song in songs if song.get('song')]
        songs = gevent.joinall(threads)
        return [i.value for i in songs if i.value]

    @staticmethod
    def _parse_one_song(song):
        lyrics = GeniusAPI.get_lyrics_list(GetMetadata.URL + song.get('path'))
        if lyrics is None:
            return {'error_id': song.get('id')}
        main_author = Author.objects.get(genius_id=song.get('primary_artist', {}).get('id'))
        if not main_author:
            return {'error_id': song.get('id')}
        media = song.get('media', [])
        spotify = [i.get('url') for i in media if i.get('provider') == 'spotify']
        youtube = [i.get('url') for i in media if i.get('provider') == 'youtube']
        parsed_song = {
            'main_author': main_author,
            'featured_authors_nicknames': [i.get('name') for i in song.get('featured_artists', []) if i.get('name')],
            'album': song.get('album').get('full_title', '') if song.get('album') else '',
            'title': song.get('title', ''),
            'lyrics': lyrics,
            'is_hot': True if song.get('stats') and song.get('stats').get('hot') else False,
            'published_date': song.get('release_date'),
            'spotify_url': spotify[0] if spotify else '',
            'youtube_url': youtube[0] if youtube else '',
            'genius_id': song.get('id'),
            'image_url': song.get('song_art_image_url', '')
        }
        return parsed_song

    @staticmethod
    def _parse_authors(authors):
        threads = [gevent.spawn(GeniusAPI._parse_one_author, author.get('artist'))
                   for author in authors if author.get('artist')]
        authors = gevent.joinall(threads)
        return [i.value for i in authors if i.value]

    @staticmethod
    def _parse_one_author(author):
        parsed_author = {
            'nickname': author.get('name'),
            'alternate_nicknames': author.get('alternate_names'),
            'facebook_name': author.get('facebook_name'),
            'instagram_name': author.get('instagram_name'),
            'twitter_name': author.get('twitter_name'),
            'genius_id': author.get('id'),
            'image_url': author.get('image_url', '')
        }
        return parsed_author

    @staticmethod
    def _handle_ommited_values(data_type, correct_parsed_data):
        omitted_data = [{'data_type': data_type, 'genius_id': data.get('error_id')}
                        for data in correct_parsed_data if data.get('error_id')]
        insert_data_to_database(OmittedData, omitted_data)
        correct_parsed_data = [data for data in correct_parsed_data if data.get('error_id') is None]
        return correct_parsed_data
