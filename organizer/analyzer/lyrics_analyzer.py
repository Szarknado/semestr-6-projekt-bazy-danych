import numpy as np
from PIL import Image


def create_similarity_image(lyrics: str, song_title, check):
    if check != '(None, None)':
        arr = np.array(np.apply_along_axis(lambda x: x == lyrics.split(), 0,
                                   np.tile(lyrics.split(), (lyrics.count(' ') + 1, 1))).tolist())*250
        pic = Image.fromarray(arr.astype('uint8'))
        path = '/media/'+song_title+'.png'
        pic.convert('L').save('media/'+song_title+'.png')
        return path
    else:
        return None


if __name__ == '__main__':
    create_similarity_image('halo its me halo i know its me')