from django.db import models
from django.contrib.postgres.fields import ArrayField


class Author(models.Model):
    genius_id = models.IntegerField(primary_key=True, unique=True)
    nickname = models.CharField(max_length=255, unique=True, db_index=True)
    alternate_nicknames = ArrayField(models.CharField(max_length=255))
    facebook_name = models.CharField(max_length=255, null=True, blank=True)
    instagram_name = models.CharField(max_length=255, null=True, blank=True)
    twitter_name = models.CharField(max_length=255, null=True, blank=True)
    image_url = models.URLField(max_length=1023)
    searcher_learned = models.IntegerField(default=0, db_index=True)
    views = models.IntegerField(default=0, db_index=True)

    class Meta:
        ordering = ('genius_id', 'nickname')

    def __str__(self):
        return self.nickname


class Songs(models.Model):
    main_author = models.ForeignKey(Author, models.CASCADE)
    featured_authors_nicknames = ArrayField(models.CharField(max_length=255))
    album = models.CharField(max_length=255, db_index=True)
    title = models.CharField(max_length=255, db_index=True)
    lyrics = models.TextField()
    published_date = models.DateField(null=True, blank=True, db_index=True)
    youtube_url = models.URLField(max_length=1023)
    spotify_url = models.URLField(max_length=1023)
    image_url = models.URLField(max_length=1023)
    is_hot = models.BooleanField(default=False)
    genius_id = models.IntegerField(primary_key=True, unique=True)
    searcher_learned = models.IntegerField(default=0, db_index=True)
    views = models.IntegerField(default=0, db_index=True)

    class Meta:
        ordering = ['main_author__name', '-published_date']
        get_latest_by = 'published_date'

    def __str__(self):
        return f"{self.title} - {self.main_author.nickname}"


class OmittedData(models.Model):
    data_type = models.CharField(max_length=64, db_index=True)
    genius_id = models.IntegerField(db_index=True)
