from django.forms import Form, CharField


class SearchFrom(Form):
    searcher = CharField(label='searcher', max_length=100)
