from django.db import connection
from organizer.analyzer.lyric_downloader import GeniusAPI
from database_resources.queries import (QUERY_SEARCH_AUTHORS, QUERY_SEARCH_SONGS,
                                        QUERY_NEW_SEARCH_AUTHORS, QUERY_NEW_SEARCH_SONGS,
                                        QUERY_INCREMENT_AUTHORS_LEARNED, QUERY_INCREMENT_SONGS_LEARNED)
from database_resources.database_inserter import named_tuple_fetchall
from .searcher.search_model import SearchModel
import numpy as np
from more_itertools import unique_everseen, powerset
from itertools import product


class Searcher:
    _fragile_signs = ['$', '(', ')', '[', ']']

    _author_search_fields = ['oa.nickname', 'alternate_nickname']
    _author_r_search_fields = ['LOWER(r.nickname)', 'LOWER(r.alternate_nickname)']
    _author_search_model = SearchModel('author')

    _songs_search_fields = ['os.title', 'os.album', 'oa.nickname', 'other_nickname']
    _songs_r_search_fields = ['LOWER(r.title)', 'LOWER(r.album)', 'LOWER(r.nickname)', 'LOWER(r.other_nickname)']
    _songs_search_model = SearchModel('songs')

    def search_phrase(self, search_phrase, download_searcher, limit):
        if download_searcher:
            GeniusAPI.search_songs(search_phrase, 2*limit)
            self.update_search_model(limit)
        author_results = self._run_search_query(self._author_search_model.get_most_similar(search_phrase, limit),
                                                self._author_search_fields, self._author_r_search_fields,
                                                QUERY_SEARCH_AUTHORS, limit, search_phrase)
        songs_results = self._run_search_query(self._songs_search_model.get_most_similar(search_phrase, limit),
                                               self._songs_search_fields, self._songs_r_search_fields,
                                               QUERY_SEARCH_SONGS, limit, search_phrase)
        return list(unique_everseen(author_results)), list(unique_everseen(songs_results))

    def _run_search_query(self, similar_search_phrases, search_fields, r_search_fields, query_template, limit, search_phrase):
        for sign in self._fragile_signs:
            search_phrase = search_phrase.replace(sign, '')
            similar_search_phrases = [sim_phrase.replace(sign, '') for sim_phrase in similar_search_phrases]
        similar_search_phrases = ', '.join([f"$${i}$$" for i in similar_search_phrases])
        search_filter_in_list = " OR ".join(f"""{field} = ANY(ARRAY[{similar_search_phrases}])""" for field in search_fields)
        search_filter_contains = " OR ".join(f"""{field} ILIKE $$%{search_part}%$$"""
                                             for field, search_part
                                             in product(search_fields, [search_phrase, *[' '.join(i) for i in list(powerset(search_phrase.split()))[1:-1]]]))
        r_search_fields_merger = ' OR '.join(f"{field} SIMILAR TO CONCAT('(% |)', sorter.arr_val, '(| %)')" for field in r_search_fields)
        search_filter = f"{search_filter_in_list} OR {search_filter_contains}"
        query = query_template.format(filtering=search_filter, limit=limit,
                                      search_phrases=f"$${search_phrase.lower()}$$, $${'$$, $$'.join(search_phrase.lower().split())}$$, {similar_search_phrases}",
                                      r_search_fields=', '.join(r_search_fields),
                                      r_search_fields_merger=r_search_fields_merger)
        with connection.cursor() as cursor:
            cursor.execute(query)
            return named_tuple_fetchall(cursor)

    def update_search_model(self, limit):
        with connection.cursor() as cursor:
            self._update_one_model(cursor, QUERY_NEW_SEARCH_AUTHORS, QUERY_INCREMENT_AUTHORS_LEARNED,
                                   self._author_search_model, 4*limit)
            self._update_one_model(cursor, QUERY_NEW_SEARCH_SONGS, QUERY_INCREMENT_SONGS_LEARNED,
                                   self._songs_search_model, 4*limit)

    @staticmethod
    def _update_one_model(cursor, query_learn, query_mark, model, limit):
        cursor.execute(query_learn.format(limit=limit))
        data = np.array(cursor.fetchall())
        if data.size:
            if model.exists():
                model.train_model(data[:, 1:].tolist())
            else:
                model.create_new_model(data[:, 1:].tolist())
            cursor.execute(query_mark.format(genius_ids=data[:, 0].astype(int).tolist()))
