import os

from gensim.models import FastText

_MODEL_FILE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "model/{model_type}/fast_text.model")


class SearchModel:
    def __init__(self, model_type):
        self._model = None
        self._model_path = _MODEL_FILE_PATH.format(model_type=model_type)

    @property
    def model(self):
        if self._model is None:
            self._model = FastText.load(self._model_path)
        return self._model

    def exists(self):
        return os.path.exists(self._model_path)

    def create_new_model(self, vectors_of_words):
        vectors_of_words = self._prepare_words(vectors_of_words)
        self._model = FastText(vectors_of_words, size=80, window=4, min_count=1, workers=os.cpu_count(), iter=20)
        self._model.epochs = 3
        self._model.save(self._model_path)

    def train_model(self, new_vectors_of_words):
        new_vectors_of_words = self._prepare_words(new_vectors_of_words)
        self.model.build_vocab(new_vectors_of_words, update=True)
        self.model.train(new_vectors_of_words, total_examples=len(new_vectors_of_words), epochs=self._model.epochs)

    def get_most_similar(self, phrase, limit):
        return [i[0] for word in phrase.split()
                for i in self.model.wv.most_similar(word.lower(), topn=limit)]

    @staticmethod
    def _prepare_words(words):
        return [[part.lower() for word in sentence for part in word.split()] for sentence in words]
