from django import template

register = template.Library()


@register.filter
def convert_spotify_url(value):
    return value.replace("https://open.spotify.com", "https://open.spotify.com/embed")
