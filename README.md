# Instruction
Delete files from organizer/migration and users/migrations
except \_\___init.py__\_\_ file.

Create database named __piosenki_db__
and grant all priviledges to an user.

Run command as postgres superuser:  
`psql piosenki_db -c 'create extension hstore;'`

Add user credentials to __DB_USER__ and __DB_PASS__.
  
Run following commands:
- `python manage.py makemigrations`
- `python manage.py migrate`
- `python build_triggers.py`
- `python manage.py runserver`





