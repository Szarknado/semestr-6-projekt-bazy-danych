from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from .forms import ImageUploadForm
from django.contrib.postgres.fields import HStoreField


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='profile_pics', default='kot_default.webp')
    liked_songs = ArrayField(models.IntegerField(), default=list, blank=True)
    liked_authors = ArrayField(models.IntegerField(), default=list, blank=True)
    authors_views = HStoreField(default=dict)
    songs_views = HStoreField(default=dict)

    def __str__(self):
        return f"{self.user.username} Profile"