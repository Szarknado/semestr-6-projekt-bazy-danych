from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from users.forms import UserRegisterForm
from .forms import ImageUploadForm
from .models import Profile
from organizer.views import handle_base_cards, go_to_song_or_author


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            messages.success(request, f"Account created successfully. User '{username}' logged in")
            return redirect('mjuzik-home')
    else:
        form = UserRegisterForm()
    if request.method == 'GET':
        song_or_author_request = go_to_song_or_author(request)
        if song_or_author_request:
            return handle_base_cards(request, song_or_author_request[0], song_or_author_request[1])
    return handle_base_cards(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            m = Profile.objects.get(user_id=request.user.id)
            m.image = form.cleaned_data['image']
            m.save()
    if request.method == 'GET':
        song_or_author_request = go_to_song_or_author(request)
        if song_or_author_request:
            return handle_base_cards(request, song_or_author_request[0], song_or_author_request[1])
    return handle_base_cards(request, 'users/profile.html', {})
