from django.db import transaction
from collections import namedtuple


@transaction.atomic
def insert_data_to_database(database, data):
    for row in data:
        entry = database(**row)
        entry.save()


def named_tuple_fetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
