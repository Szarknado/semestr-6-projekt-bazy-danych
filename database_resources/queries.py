QUERY_TOP_AUTHORS = """
SELECT genius_id, image_url, nickname, alternate_nicknames
FROM organizer_author
ORDER BY views DESC
LIMIT {limit};
"""

QUERY_TOP_AUTHORS_USER = """
SELECT genius_id, image_url, nickname, alternate_nicknames
FROM organizer_author
RIGHT JOIN (SELECT * FROM users_profile WHERE user_id = {user_id}) AS p
ON genius_id = ANY(p.liked_authors)
WHERE genius_id IS NOT NULL
ORDER BY p.authors_views -> CAST(genius_id AS VARCHAR) DESC
LIMIT {limit};
"""

QUERY_TOP_SONGS = """
SELECT os.genius_id, os.image_url, os.title, oa.nickname, os.album
FROM organizer_songs AS os 
INNER JOIN organizer_author AS oa 
ON os.main_author_id = oa.genius_id 
ORDER BY os.views DESC 
LIMIT {limit};
"""

QUERY_TOP_SONGS_USER = """
SELECT os.genius_id, os.image_url, os.title, oa.nickname, os.album
FROM organizer_songs AS os 
INNER JOIN organizer_author AS oa 
ON os.main_author_id = oa.genius_id
RIGHT JOIN (SELECT * FROM users_profile WHERE user_id = {user_id}) AS p
ON os.genius_id = ANY(p.liked_songs)
WHERE os.genius_id IS NOT NULL 
ORDER BY p.songs_views -> CAST(os.genius_id AS VARCHAR) DESC
LIMIT {limit};
"""

QUERY_NEW_SEARCH_AUTHORS = """
SELECT oa.genius_id, oa.nickname, alternate_nickname
FROM organizer_songs AS os 
INNER JOIN organizer_author AS oa 
ON os.main_author_id = oa.genius_id, unnest(oa.alternate_nicknames) AS alternate_nickname
WHERE os.published_date IS NOT NULL AND os.published_date < NOW() 
GROUP BY oa.genius_id, oa.nickname, alternate_nickname
ORDER BY oa.searcher_learned ASC, MAX(os.published_date) DESC 
LIMIT {limit};
"""

QUERY_INCREMENT_AUTHORS_LEARNED = """
UPDATE organizer_author
SET searcher_learned = searcher_learned + 1
WHERE genius_id = ANY(ARRAY{genius_ids})
"""

QUERY_NEW_SEARCH_SONGS = """
SELECT os.genius_id, os.title, oa.nickname, os.album, other_nickname
FROM organizer_songs AS os 
INNER JOIN organizer_author AS oa 
ON os.main_author_id = oa.genius_id, unnest(os.featured_authors_nicknames) AS other_nickname 
WHERE os.published_date IS NOT NULL AND os.published_date < NOW()
GROUP BY os.genius_id, os.title, oa.nickname, os.album, other_nickname
ORDER BY os.searcher_learned ASC, os.published_date DESC 
LIMIT {limit};
"""

QUERY_INCREMENT_SONGS_LEARNED = """
UPDATE organizer_songs
SET searcher_learned = searcher_learned + 1
WHERE genius_id = ANY(ARRAY{genius_ids})
"""


QUERY_SEARCH_AUTHORS = """
CREATE SEQUENCE IF NOT EXISTS author_counter start 1;

WITH to_order_result AS (
WITH result AS (
SELECT oa.genius_id, oa.image_url, oa.nickname, alternate_nickname
FROM organizer_songs AS os 
INNER JOIN organizer_author AS oa 
ON os.main_author_id = oa.genius_id,
unnest(CASE WHEN oa.alternate_nicknames IS NULL OR array_length(oa.alternate_nicknames,1) IS NULL THEN ARRAY['']
ELSE oa.alternate_nicknames
END) AS alternate_nickname
WHERE os.published_date IS NOT NULL AND os.published_date < NOW() AND ({filtering}) 
GROUP BY oa.genius_id, oa.image_url, oa.nickname, alternate_nickname
)
SELECT sorter.arr_sorter, r.genius_id, r.image_url, r.nickname
FROM result AS r
JOIN (SELECT unnest(ARRAY[{search_phrases}]) AS arr_val, nextval('author_counter') AS arr_sorter) AS sorter
ON {r_search_fields_merger}
)
SELECT genius_id, MAX(image_url) AS image_url, nickname
FROM to_order_result
GROUP BY genius_id, nickname, arr_sorter
ORDER BY arr_sorter
LIMIT {limit};
"""

QUERY_SEARCH_SONGS = """
CREATE SEQUENCE IF NOT EXISTS songs_counter start 1;

WITH to_order_result AS (
WITH result AS (
SELECT os.genius_id, os.image_url, os.title, oa.nickname, os.album, other_nickname
FROM organizer_songs AS os 
INNER JOIN organizer_author AS oa 
ON os.main_author_id = oa.genius_id,
unnest(CASE WHEN os.featured_authors_nicknames IS NULL OR array_length(os.featured_authors_nicknames,1) IS NULL THEN ARRAY['']
ELSE os.featured_authors_nicknames
END) AS other_nickname 
WHERE os.published_date IS NOT NULL AND os.published_date < NOW() AND ({filtering})
GROUP BY os.genius_id, os.image_url, os.title, oa.nickname, os.album, other_nickname
)
SELECT sorter.arr_sorter, r.genius_id, r.image_url, r.title, r.nickname, r.album
FROM result AS r
JOIN (SELECT unnest(ARRAY[{search_phrases}]) AS arr_val, nextval('songs_counter') AS arr_sorter) AS sorter
ON {r_search_fields_merger}
)
SELECT genius_id, image_url, title, nickname, album
FROM to_order_result
GROUP BY genius_id, image_url, title, nickname, album, arr_sorter
ORDER BY arr_sorter
LIMIT {limit};
"""
